package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      if (c == 0 && r == 0) {
        1
      } else if ((c-1) < 0) {
        pascal(c, r - 1)
      } else if (c > (r-1)) {
        pascal(c-1, r - 1)
      } else {
        pascal(c-1, r - 1) + pascal(c, r - 1)
      }
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
      stringBalance(chars, 0)
    }

    private def stringBalance(chars: List[Char], validity: Int): Boolean = {
      if (validity < 0) {
        false
      } else if (chars.isEmpty) {
        if (validity.equals(0)) {
          true
        } else {
          false
        }
      } else {
        val head = chars.head

        if (head.equals('(')) {
          stringBalance(chars.tail, validity + 1)
        } else if (head.equals(')')) {
          stringBalance(chars.tail, validity - 1)
        } else {
          stringBalance(chars.tail, validity)
        }
      }
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      var outputArray = new Array[Int](money + 1)

      outputArray(0) = 1
      countChangesRecurse(coins, outputArray)

      outputArray(money)
    }

    private def countChangesRecurse(coins: List[Int], outputArray: Array[Int]): Unit = {
      if (coins.isEmpty) return
      else {
        val head = coins.head
        var index = 1

        while (index < outputArray.length) {
          if (index - head >= 0) {
            outputArray(index) = outputArray(index) + outputArray(index - head)
          }
          index += 1
        }
        countChangesRecurse(coins.tail, outputArray)
      }
    }
  }
